﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System.Net.Sockets;
using System;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;

public class VIPLENetworkController : MonoBehaviour {

    private static TcpListener tcpListener;
    private Thread tcpListenerThread;
    private TcpClient tcpContext = null;
    private String incomingTcpMessage;
    System.IO.Stream output;

    public GameObject player;

    private RobotBean robotBean;


    public float[] servoValues;

    bool forwardMessage;
    bool leftTurnMessage;
    bool rightTurnMessage;

    public float rightSensorValue;
    public float frontSensorValue;
    public float leftSensorValue;
    public float backSensorValue;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        forwardMessage = false;
        leftTurnMessage = false;
        rightTurnMessage = false;

        rightSensorValue = 0.0f;
        frontSensorValue = 0.0f;
        leftSensorValue = 0.0f;
        backSensorValue = 0.0f;

        // Get command line parameters
        int tcpPortInput = 1350;
        tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), tcpPortInput);
        tcpListener.Start();



        this.tcpListenerThread = new Thread(new ParameterizedThreadStart(startTcpListener));
        tcpListenerThread.Start();
    }

    /* TCP CONNECTION HANDLERS*/
    private void startTcpListener(object s)
    {
        StateObjClass StateObj = new StateObjClass();
        StateObj.TimerCanceled = false;

        System.Threading.TimerCallback TimerDelegate = new System.Threading.TimerCallback(sendSensorUpdateProfile);
        System.Threading.Timer TimerItem = new System.Threading.Timer(TimerDelegate, null, 0, 260);
        StateObj.TimerReference = TimerItem;

        while (true)
        {

            if (tcpListener.Pending())
            {
                IAsyncResult result = tcpListener.BeginAcceptTcpClient(TcpListenerCallback, tcpListener);
                result.AsyncWaitHandle.WaitOne(250);
            }
            else
            {
                Thread.Sleep(100);
            }
        }
    }

    private void TcpListenerCallback(IAsyncResult result)
    {
        int count = 0;
        // read incoming tcp message
        tcpContext = tcpListener.EndAcceptTcpClient(result);
        StreamReader tcpStreamReader = new StreamReader(tcpContext.GetStream());
        while (true)
        {
            incomingTcpMessage = tcpStreamReader.ReadLine().Trim();
            /* TODO: Support more than 2 servos & take in any actuators*/

            int currentId = -1;
            count = 0;
            bool willTurnWholeRobot = false;
            bool isRobotDrive = false;
            float degreesToTurnRobot = 0;

            /* Parse incoming tcp message from JSON */
            Debug.Log(incomingTcpMessage);

            /* Parse incoming tcp message from JSON */
            Newtonsoft.Json.JsonTextReader jsonTextReader = new Newtonsoft.Json.JsonTextReader(new StringReader(incomingTcpMessage));
            while (jsonTextReader.Read())
            {
                Debug.Log(jsonTextReader.TokenType.ToString());
                // READ SERVO INPUTS
                if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("servos"))
                {
                    while (!jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.EndArray))
                    {
                        // Get if speed value is a turn
                        if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("isTurn"))
                        {
                            jsonTextReader.Read();
                            if (jsonTextReader.Value != null)
                            {
                                bool isTurn = bool.Parse(jsonTextReader.Value.ToString());
                                // servo id = -1; turn the robot at specified degree via servoSpeed
                                if (currentId == -1)
                                {
                                    if (isTurn)
                                    {
                                        willTurnWholeRobot = true;
                                    }
                                    else
                                    {
                                        willTurnWholeRobot = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            jsonTextReader.Read();
                        }

                        // Get ServoId
                        if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("servoId"))
                        {
                            jsonTextReader.Read();
                            if (jsonTextReader.Value != null && jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.Integer))
                            {
                                //currentId = int.Parse(jsonTextReader.Value.ToString()) % 2; //  TODO: Fix action profile maping to 0 and 1 index
                                if (int.Parse(jsonTextReader.Value.ToString()) == -1)
                                {
                                    isRobotDrive = false;
                                    currentId = -1;
                                }

                                else
                                {
                                    isRobotDrive = true;
                                    currentId = count;
                                    count++;
                                }

                            }
                        }
                        // Get ServoSpeed
                        if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("servoSpeed"))
                        {
                            jsonTextReader.Read();
                            if (jsonTextReader.Value != null)
                            {
                                // servo id = -1; turn the robot at specified degree via servoSpeed
                                if (currentId == -1)
                                {
                                    degreesToTurnRobot = float.Parse(jsonTextReader.Value.ToString());
                                }
                                else
                                {
                                    float servoSpeed = float.Parse(jsonTextReader.Value.ToString());
                                    servoValues[currentId] = (float)servoSpeed;

                                }

                            }
                        }

                    }// end servos array
                } // end servos property
            }// end json reader

            Debug.Log(degreesToTurnRobot);
            if (!willTurnWholeRobot)
            {
                forwardMessage = true;
            }
            else if (degreesToTurnRobot > 0)
            {
                rightTurnMessage = true;
            }
            else if (degreesToTurnRobot < 0)
            {
                leftTurnMessage = true;
            }

            output = tcpContext.GetStream();

            /*
            * OPEN OUTPUT STREAM AND WRITE ROBOT STATUS
            String output2 = robotBean.toJSON() + "\n";
            Debug.Log("output2" + output2);
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(robotBean.toJSON().Trim() + "\n");
            output = tcpContext.GetStream();
            output.Write(buffer, 0, buffer.Length);
            */

        }
    }

    public void setSensor(int index, float value)
    {
        Debug.Log("Changing sensor" + index + "," + value);

        if (index == 0)
        {
            rightSensorValue = value;
        }
        else if (index == 1)
        {
            frontSensorValue = value;
        }
        else if (index == 2)
        {
            leftSensorValue = value;
        }
        else if (index == 3)
        {
            backSensorValue = value;
        }
    }

    public string getSensorJson()
    {
        StringBuilder stringBuilder = new StringBuilder();
        StringWriter stringWriter = new StringWriter(stringBuilder);
        JsonWriter jsonWriter = new JsonTextWriter(stringWriter);
        JsonWriter writer = new JsonTextWriter(stringWriter);

        writer.Formatting = Formatting.Indented;
        writer.WriteStartObject();
        writer.WritePropertyName("sensors");
        writer.WriteStartArray();

        //Start printing sensor profile
        for (int i = 0; i < 3; i++)
        {
            writer.WriteStartObject();

            writer.WritePropertyName("name");
            writer.WriteValue("distance");

            writer.WritePropertyName("id");
            writer.WriteValue(i);
            

            writer.WritePropertyName("value");

            if (i == 0)
            {
                writer.WriteValue(rightSensorValue);
            }
            else if (i == 1)
            {
                writer.WriteValue(frontSensorValue);
            }
            else if (i == 2)
            {
                writer.WriteValue(leftSensorValue);
            }
            else if (i == 3)
            {
                writer.WriteValue(backSensorValue);
            }

            writer.WriteEndObject();
        }

        writer.WriteEndArray();

        writer.WriteEndObject();
        String jsonOutput = stringWriter.ToString();
        jsonOutput = jsonOutput.Replace("\r", "");
        jsonOutput = jsonOutput.Replace("\n", "");
        return jsonOutput;
    }

    private void sendSensorUpdateProfile(System.Object state)
    {
        StateObjClass stateObjClass = (StateObjClass)state;
        if (tcpContext != null)
        {
            String strRobot = Regex.Replace(getSensorJson(), @"\s+", "");
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(strRobot + "\n");

            output.Write(buffer, 0, buffer.Length);
        }
        else
        {
            stateObjClass.TimerReference.Dispose();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (forwardMessage)
        {
            player.SendMessage("RobotForward");
            forwardMessage = false;
        }
        else if (rightTurnMessage)
        {
            player.SendMessage("RobotRightTurn");
            rightTurnMessage = false;
        }
        else if (leftTurnMessage)
        {
            player.SendMessage("RobotLeftTurn");
            leftTurnMessage = false;
        }

    }

    public void closeTCP()
    {
        tcpListener.Stop();
    }

    private class StateObjClass
    {
        // Used to hold parameters for calls to TimerTask.
        //public int SomeValue;
        public System.Threading.Timer TimerReference;
        public bool TimerCanceled;
    }
}
