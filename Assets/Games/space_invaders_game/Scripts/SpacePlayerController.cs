﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}


public class SpacePlayerController : MonoBehaviour {

    public float fireRate = 0.6f;
    public float speed;
    public GameObject projectile;
    public Transform projectileSpawner;
    public Boundary boundary;

    // from unity tutorial space shooter
    private float myTime = 0.0F;
    private float nextFire = 0.5F;
    private Rigidbody2D rb;
    private AudioSource audioSource;

    bool shiftLeftFlag;
    bool shiftRightFlag;
    bool laserFlag;

    // Use this for initialization
    void Start ()
    {
        shiftLeftFlag = false;
        shiftRightFlag = false;
        laserFlag = false;

        rb = GetComponent<Rigidbody2D>();
	}

    public void RobotRightTurn()
    {
        Debug.Log("Received right turn message.");
        ShiftRight();
    }

    public void RobotLeftTurn()
    {
        Debug.Log("Received left turn message.");
        ShiftLeft();
    }
    
    public void RobotForward()
    {
        Debug.Log("Received forward message.");
        FireLaser();
    }

    public void FireLaser()
    {
        laserFlag = true;
    }

    public void ShiftLeft()
    {
        shiftLeftFlag = true;
    }

    public void ShiftRight()
    {
        shiftRightFlag = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(projectile, projectileSpawner.position, projectileSpawner.rotation);
        }

        if (laserFlag && Time.time > nextFire)
        {
            Debug.Log("fired laser by VIPLE command");
            nextFire = Time.time + fireRate;
            Instantiate(projectile, projectileSpawner.position, projectileSpawner.rotation);
            laserFlag = false;
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        if (shiftLeftFlag)
        {
            moveHorizontal = -1f;
            shiftLeftFlag = false;
        }
        else if (shiftRightFlag)
        {
            moveHorizontal = 1f;
            shiftRightFlag = false;
        }

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        rb.velocity = movement * speed;

        rb.position = new Vector2
        (
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax)
        );
    }
}
