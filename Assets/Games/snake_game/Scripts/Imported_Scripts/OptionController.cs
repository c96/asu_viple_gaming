﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class OptionController : MonoBehaviour
{

    public Scene scene;

    public Dropdown defaultDropdown;
    public int default1;
    public Button addButton;
    public Button deleteButton;
    public int hideCnt;

    public Dropdown ifButton1;
    public int if1;
    public InputField input1;
    public float value1;
    public Dropdown funcButton1;
    public int func1;

    public Dropdown ifButton2;
    public int if2;
    public InputField input2;
    public float value2;
    public Dropdown funcButton2;
    public int func2;

    public Dropdown ifButton3;
    public int if3;
    public InputField input3;
    public float value3;
    public Dropdown funcButton3;
    public int func3;

    public Dropdown ifButton4;
    public int if4;
    public InputField input4;
    public float value4;
    public Dropdown funcButton4;
    public int func4;

    public Button executeButton;
    public Button pauseButton;

    public Text text2;
    public Text text3;
    public Text text4;

    //RobotController derp;

    public RobotController derp;

    void OnEnable()
    {

        //GameObject currentRobot;
        if (scene.name == "Level01")
        {
            Debug.Log("11111111111111111111111111111111111111");
            derp = (RobotController)GameObject.Find("Robot Kyle").GetComponent("RobotController");
        }
        else
        {
            //Debug.Log("2222222222222222222222222222222222222");
            derp = (RobotController)GameObject.Find("Robot").GetComponent("RobotController");
        }

        defaultDropdown.onValueChanged.AddListener(delegate { onDefaultDropdown(); });
        addButton.onClick.AddListener(delegate { onAddButton(); });

        deleteButton.onClick.AddListener(delegate { onDeleteButton(); });
        deleteButton.interactable = false;

        executeButton.onClick.AddListener(delegate { onExecuteButton(); });
        pauseButton.onClick.AddListener(delegate { onPauseButton(); });

        ifButton1.onValueChanged.AddListener(delegate { onIfDropdown1(); });
        funcButton1.onValueChanged.AddListener(delegate { onFuncDropdown1(); });

        ifButton2.onValueChanged.AddListener(delegate { onIfDropdown2(); });
        funcButton2.onValueChanged.AddListener(delegate { onFuncDropdown2(); });

        ifButton3.onValueChanged.AddListener(delegate { onIfDropdown3(); });
        funcButton3.onValueChanged.AddListener(delegate { onFuncDropdown3(); });

        ifButton4.onValueChanged.AddListener(delegate { onIfDropdown4(); });
        funcButton4.onValueChanged.AddListener(delegate { onFuncDropdown4(); });

        hiddenDropdown(ifButton1, true);
        hiddenDropdown(funcButton1, true);
        hiddenInputField(input1, true);

        hiddenDropdown(ifButton2, true);
        hiddenDropdown(funcButton2, true);
        hiddenInputField(input2, true);
        hiddenText(text2, true);

        hiddenDropdown(ifButton3, true);
        hiddenDropdown(funcButton3, true);
        hiddenInputField(input3, true);
        hiddenText(text3, true);

        hiddenDropdown(ifButton4, true);
        hiddenDropdown(funcButton4, true);
        hiddenInputField(input4, true);
        hiddenText(text4, true);
        hideCnt = 0;
    }
    public void onAddButton()
    {

        if (hideCnt == 0)
        {
            hiddenDropdown(ifButton1, false);
            hiddenDropdown(funcButton1, false);
            hiddenInputField(input1, false);
        }
        if (hideCnt == 1)
        {
            hiddenDropdown(ifButton2, false);
            hiddenDropdown(funcButton2, false);
            hiddenInputField(input2, false);
            hiddenText(text2, false);
        }
        if (hideCnt == 2)
        {
            hiddenDropdown(ifButton3, false);
            hiddenDropdown(funcButton3, false);
            hiddenInputField(input3, false);
            hiddenText(text3, false);
        }
        if (hideCnt == 3)
        {
            hiddenDropdown(ifButton4, false);
            hiddenDropdown(funcButton4, false);
            hiddenInputField(input4, false);
            hiddenText(text4, false);
        }
        hideCnt++;
        deleteButton.interactable = true;
    }
    public void onDeleteButton()
    {
        if(hideCnt == 1)
        {
            hiddenDropdown(ifButton1, true);
            hiddenDropdown(funcButton1, true);
            hiddenInputField(input1, true);
        }
        if (hideCnt == 2)
        {
            hiddenDropdown(ifButton2, true);
            hiddenDropdown(funcButton2, true);
            hiddenInputField(input2, true);
            hiddenText(text2, true);
        }
        if (hideCnt == 3)
        {
            hiddenDropdown(ifButton3, true);
            hiddenDropdown(funcButton3, true);
            hiddenInputField(input3, true);
            hiddenText(text3, true);
        }
        if (hideCnt == 3)
        {
            hiddenDropdown(ifButton4, true);
            hiddenDropdown(funcButton4, true);
            hiddenInputField(input4, true);
            hiddenText(text4, true);
        }
        hideCnt--;
        if (hideCnt == 0)
        {
            deleteButton.interactable = false;
        }
        else
        {
            deleteButton.interactable = true;
        }
    }

    public void onDefaultDropdown()
    {
        //derp.setManProfile(defaultDropdown.value);
        default1 = defaultDropdown.value;
        //Debug.Log(defaultDropdown.value);
        return;
    }

    public void onIfDropdown1()
    {
        if1 = ifButton1.value;
    }
    public void onFuncDropdown1()
    {
        func1 = funcButton1.value;
    }

    public void onIfDropdown2()
    {
        if2 = ifButton2.value;
    }
    public void onFuncDropdown2()
    {
        func2 = funcButton2.value;
    }

    public void onIfDropdown3()
    {
        if3 = ifButton3.value;
    }
    public void onFuncDropdown3()
    {
        func3 = funcButton3.value;
    }

    public void onIfDropdown4()
    {
        if4 = ifButton4.value;
    }
    public void onFuncDropdown4()
    {
        func4 = funcButton4.value;
    }
    public void onExecuteButton()
    {
        derp.setControlFlag(true);
        //derp.setManual(true);
        //derp.setMan(1, true);
        return;
    }
    public void onPauseButton()
    {
        derp.setControlFlag(false);
        //derp.setManual(false);
        //derp.setMan(1, true);
        return;
    }

    public static void hiddenButton(Button but, bool isHidden)
    {
        if (isHidden)
        {
            but.enabled = false;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
            but.GetComponentInChildren<Text>().color = Color.clear;
        }
        else
        {
            but.enabled = true;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
            but.GetComponentInChildren<Text>().color = Color.black;
        }
    }

    public static void hiddenDropdown(Dropdown but, bool isHidden)
    {
        
        if (isHidden)
        {
            //but.enabled = false;
            but.interactable = false;
            /*
            but.enabled = false;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
            but.GetComponentInChildren<Text>().color = Color.clear;
            */
        }
        else
        {
            //but.enabled = true;
            but.interactable = true;
            //but.colors = Color.black;
            /*
            but.enabled = true;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
            but.GetComponentInChildren<Text>().color = Color.black;
            */
        }
    }

    public static void hiddenText(Text but, bool isHidden)
    {
        if (isHidden)
        {
            but.enabled = false;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
            but.GetComponentInChildren<Text>().color = Color.clear;
        }
        else
        {
            but.enabled = true;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
            but.GetComponentInChildren<Text>().color = Color.black;
        }
    }

    public static void hiddenInputField(InputField but, bool isHidden)
    {
        if (isHidden)
        {
            but.enabled = false;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
            but.GetComponentInChildren<Text>().color = Color.clear;
        }
        else
        {
            but.enabled = true;
            but.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
            but.GetComponentInChildren<Text>().color = Color.black;
        }
    }

    public void text_changed1(string newText)
    {
        value1 = float.Parse(newText);
    }
    public void text_changed2(string newText)
    {
        value2 = float.Parse(newText);
    }
    public void text_changed3(string newText)
    {
        value3 = float.Parse(newText);
    }
    public void text_changed4(string newText)
    {
        value4 = float.Parse(newText);
    }

    public int getHideCnt()
    {
        return hideCnt;
    }

    public int getIf1()
    {
        return if1;
    }

    public float getValue1()
    {
        return value1;
    }

    public int getFunc1()
    {
        return func1;
    }
}
