﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class RobotData : MonoBehaviour
{

    public string time;

    public string OS;
    public string deviceType;
    public string graphicsDeviceName;
    public string graphicsDeviceVersion;
    public string processor;
    public int processorCount;
    public int systemMemory;

    public string sessionID;
    public string clientID;


    public string ipAddress;
    public float latitude;
    public float longitude;
    public string city;
    public string state;
    public string country;

    public List<Vector3> coords;


}
