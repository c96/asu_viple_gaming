﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;

/// <summary>
/// Handle the game object representing the robot
/// </summary>
public class RobotController : MonoBehaviour
{
    //=====================================================================
    private bool IsManual = false;
	private int manProfile = 0;
    public bool controlFlag = false;


    public bool delayFlag = false;

    public void setMan(int Man, bool Is)
    {
        manProfile = Man;
        IsManual = Is;
    }
    public void setControlFlag(bool Is)
    {
        controlFlag = Is;
    }


    public OptionController optionClass;


    public Transform gridStartPoint;
    public GameObject spawnPlane_MazeFloor;
    public GameObject spawnCube_MazeWall;
    public System.Object[][] mazeFloor;
    public const int row = 14;
    public const int col = 22;
    public bool[,] mazeLayout = new bool[row, col] {    { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true },
                                                        { true, false, false, false, true, true, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, true },
                                                        { true, false, false, false, true, true, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false ,true },
                                                        { true, false, false, false, true, true, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false ,true },
                                                        { true, false, false, false, true, true, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true },
                                                        { true, false, false, false, true, true, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true },
                                                        { true, false, false, false, true, true, false, false, true, true, false, false, true, true, false, false, false, false, false, false, false, true },
                                                        { true, false, false, false, true, true, false, false, true, true, false, false, true, true, false, false, false, false, false, false, false, true },
                                                        { true, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, true, true, false, false, false, true },
                                                        { true, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, true, true, false, false, false, true },
                                                        { true, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, true},
                                                        { true, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, true},
                                                        { true, true,  true, true,  true, true, true, true, true, true, true, true, true,  true, true,  true, true, true, true, true, true, true },
                                                        { true, true,  true, true,  true, true, true, true, true, true, true, true, true,  true, true,  true, true, true, true, true, true, true }};
    //=============================================================================
    public Scene scene;
    public bool sceneFlag = true;
    public Camera cam1,cam2,cam3;
    //public bool tcp = true;

    public float movementSpeed = 0.0f;
    private const int ACTION_PROFILE_STOP = 0;
    private const int ACTION_PROFILE_FORWARD = 1;
    private const int ACTION_PROFILE_BACKWARD = 2;
    private const int ACTION_PROFILE_LEFT = 3;
    private const int ACTION_PROFILE_RIGHT = 4;
    private const int ACTION_PROFILE_TURN = 5;
    private const int ACTION_PROFILE_TURNHEAD = 6;
    private const int ACTION_PROFILE_ROBOTDRIVE = 7;
	public float[] servoValues;
    private int sensorUpdateTimeout = 300; // time to send updates about sensors

    private int currentControlIndex = 0;
    private float currentTurnDegreeProfile = 0f;
    private int currentAbsoluteAngle = 0; // angle at which the robot should be set to.
    private RobotBean robotBean;

    private static TcpListener tcpListener;
    private Thread tcpListenerThread;
    private TcpClient tcpContext = null;
    private String incomingTcpMessage;
    System.IO.Stream output;

    Animator anim;
    public DistanceSensorController distanceSensorRightController;
    public DistanceSensorController distanceSensorFrontController;
    public DistanceSensorController distanceSensorReverseController;
    public DistanceSensorController distanceSensorLeftController;
    //private TouchSensorController touchSensorController;


    /// <summary>
    /// Initialize robot by creating a bean reference and opening a listener
    /// on a TCP port
    /// </summary>
    void Start()
    {
		servoValues = new float[3]{0,0,0};
        //derp = (RobotController)GameObject.Find("Robot Kyle").GetComponent("RobotController");
        //optionClass = (OptionController)GameObject.Find("Panel").GetComponent("OptionController");

        cam1 = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam2 = GameObject.Find("MidCamera").GetComponent<Camera>();
        cam3 = GameObject.Find("Following Camera").GetComponent<Camera>();
        cam1.enabled = true;
        cam2.enabled = false;
        cam2.enabled = false;
        scene = SceneManager.GetActiveScene();

        movementSpeed = 0.0f;
        anim = GetComponent<Animator>();
        // Create Bean objects
        robotBean = new RobotBean(1, "testRobot");

        // Get references to sensor game objects
        //distanceSensorRightController = GameObject.FindGameObjectWithTag("distanceSensorRight").GetComponent<DistanceSensorController>();
        //distanceSensorFrontController = GameObject.FindGameObjectWithTag("distanceSensorFront").GetComponent<DistanceSensorController>();
        //distanceSensorLeftController = GameObject.FindGameObjectWithTag("distanceSensorLeft").GetComponent<DistanceSensorController>();
        //distanceSensorReverseController = GameObject.FindGameObjectWithTag("distanceSensorReverse").GetComponent<DistanceSensorController>();

        // Get command line parameters
        String[] arguments = Environment.GetCommandLineArgs();
        int tcpPortInput = 1350;

        try
        {
            tcpPortInput = Int32.Parse(arguments[1]);
        }
        catch (FormatException e)
        {
            tcpPortInput = 1350;
            Debug.Log(tcpPortInput);
        }

        tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), tcpPortInput);
        tcpListener.Start();



        this.tcpListenerThread = new Thread(new ParameterizedThreadStart(startTcpListener));
        tcpListenerThread.Start();
    }

    /* TCP CONNECTION HANDLERS*/
    private void startTcpListener(object s)
    {
        StateObjClass StateObj = new StateObjClass();
        StateObj.TimerCanceled = false;

        System.Threading.TimerCallback TimerDelegate = new System.Threading.TimerCallback(sendSensorUpdateProfile);
        System.Threading.Timer TimerItem = new System.Threading.Timer(TimerDelegate, null, 0, 260);
        StateObj.TimerReference = TimerItem;

        while (true)
        {

            if (tcpListener.Pending())
            {
                IAsyncResult result = tcpListener.BeginAcceptTcpClient(TcpListenerCallback, tcpListener);
                sceneFlag = false;
                result.AsyncWaitHandle.WaitOne(250);
            }
            else
            {
                Thread.Sleep(100);
            }
        }
    }

    private void TcpListenerCallback(IAsyncResult result)
    {
        int count = 0;
        // read incoming tcp message
        tcpContext = tcpListener.EndAcceptTcpClient(result);
        StreamReader tcpStreamReader = new StreamReader(tcpContext.GetStream());
        while (true)
        {
            incomingTcpMessage = tcpStreamReader.ReadLine().Trim();
            Debug.Log(incomingTcpMessage);
            /* TODO: Support more than 2 servos & take in any actuators*/

            int currentId = -1;
			count = 0;
            bool willTurnWholeRobot = false;
            bool isRobotDrive = false;
            float degreesToTurnRobot = 0;

            /* Parse incoming tcp message from JSON */
            Newtonsoft.Json.JsonTextReader jsonTextReader = new Newtonsoft.Json.JsonTextReader(new StringReader(incomingTcpMessage));
            while (jsonTextReader.Read())
            {
                Debug.Log(jsonTextReader.TokenType.ToString());
                // READ SERVO INPUTS
                if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("servos"))
                {
                    while (!jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.EndArray))
                    {
                        // Get if speed value is a turn
                        if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("isTurn"))
                        {
                            jsonTextReader.Read();
                            if (jsonTextReader.Value != null)
                            {
                                bool isTurn = bool.Parse(jsonTextReader.Value.ToString());
                                // servo id = -1; turn the robot at specified degree via servoSpeed
                                if (currentId == -1)
                                {
                                    if (isTurn)
                                    {
                                        willTurnWholeRobot = true;
                                    }
                                    else
                                    {
                                        willTurnWholeRobot = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            jsonTextReader.Read();
                        }

                        // Get ServoId
                        if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("servoId"))
                        {
                            jsonTextReader.Read();
                            if (jsonTextReader.Value != null && jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.Integer))
                            {
                                //currentId = int.Parse(jsonTextReader.Value.ToString()) % 2; //  TODO: Fix action profile maping to 0 and 1 index
                                if (int.Parse(jsonTextReader.Value.ToString()) == -1)
                                {
                                    isRobotDrive = false;
                                    currentId = -1;
                                }

                                else
                                {
                                    isRobotDrive = true;
                                    currentId = count;
                                    count++;
                                }

                            }
                        }
                        // Get ServoSpeed
                        if (jsonTextReader.TokenType.Equals(Newtonsoft.Json.JsonToken.PropertyName) && jsonTextReader.Value.Equals("servoSpeed"))
                        {
                            jsonTextReader.Read();
                            if (jsonTextReader.Value != null)
                            {
                                // servo id = -1; turn the robot at specified degree via servoSpeed
                                if (currentId == -1)
                                {
                                    degreesToTurnRobot = float.Parse(jsonTextReader.Value.ToString());
                                }
                                else
                                {
                                    float servoSpeed = float.Parse(jsonTextReader.Value.ToString());
									servoValues[currentId] = (float)servoSpeed;

                                }

                            }
                        }

                    }// end servos array
                } // end servos property
            }// end json reader

            /*TODO: Map correct motor actions to proper servo values*/
            if (isRobotDrive)
            {
                currentControlIndex = ACTION_PROFILE_ROBOTDRIVE;
            }
            else if (willTurnWholeRobot)
            {
                currentControlIndex = ACTION_PROFILE_TURN;
                currentTurnDegreeProfile = degreesToTurnRobot;
            }
            else if (degreesToTurnRobot > 0)
            {
                movementSpeed = degreesToTurnRobot;
                currentControlIndex = ACTION_PROFILE_FORWARD;
            }
            else if (degreesToTurnRobot < 0)
            {
                movementSpeed = degreesToTurnRobot;
                currentControlIndex = ACTION_PROFILE_BACKWARD;
            }
            else if (degreesToTurnRobot == 0)
            {
                currentControlIndex = ACTION_PROFILE_STOP;
            }
            else
            {
                movementSpeed = degreesToTurnRobot;

                /*
				// left side + | right side - | => GO RIGHT
				if (servoValues[0] > 0 && servoValues[1] <= 0)
				{
					currentControlIndex = ACTION_PROFILE_RIGHT;
				}
				// left side - | right side + | => GO LEFT
				else if (servoValues[0] <= 0 && servoValues[1] > 0)
				{
					currentControlIndex = ACTION_PROFILE_LEFT;
				}
				// left side + | right side + | => GO FORWARD
				else if (servoValues[0] > 0 && servoValues[1] > 0)
				{
					movementSpeed = servoValues[0];
					currentControlIndex = ACTION_PROFILE_FORWARD;
				}
				// left side - | right side - | => GO BACKWARD
				else if (servoValues[0] < 0 && servoValues[1] < 0)
				{
					movementSpeed = servoValues[0];
					currentControlIndex = ACTION_PROFILE_BACKWARD;
				}
				if(servoValues[2] != 0)
				{
					currentControlIndex = ACTION_PROFILE_TURNHEAD;
				}
				*/
            }


            /*
            * OPEN OUTPUT STREAM AND WRITE ROBOT STATUS
            */
            String output2 = robotBean.toJSON() + "\n";
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(robotBean.toJSON().Trim() + "\n");
            output = tcpContext.GetStream();
            output.Write(buffer, 0, buffer.Length);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (controlFlag == true)
        {
            optionCodeControl();
            return;
        }

        if (IsManual) {
			switch (manProfile) {
			case 0:
				ActionProfile_Stop ();
				movementSpeed = 0;
				break;
			case 1:
				ActionProfile_MoveForward ();
				movementSpeed = 1;
				break;
			case 2:
				ActionProfile_MoveBackward ();
				movementSpeed = -1;
				break;
			case 3:
				StartCoroutine ("ActionProfile_TurnLeft", 90);
				movementSpeed = 1;
                manProfile = 0;
				break;
			case 4:
				StartCoroutine ("ActionProfile_TurnRight", 90);
				movementSpeed = 1;
                manProfile = 0;
				break;
			}
            if (scene.name == "Level01")  anim.SetFloat("Speed", movementSpeed);
			return;
		}
        //=============================================
        //Some script to contorl the robot in the simulator
        /*
        if (Input.GetKey("w"))
            ActionProfile_MoveForward();
        if (Input.GetKey("s"))
            //transform.Translate(new Vector3(0, 0, 0.05f));
            ActionProfile_MoveBackward();
        if (Input.GetKey("a"))
            //transform.Rotate(new Vector3(0, 2, 0));
            StartCoroutine("ActionProfile_TurnLeft", 90);
            //ActionProfile_RobotDrive(0, 1);
        if (Input.GetKey("d"))
            //transform.Rotate(new Vector3(0, -2, 0));
            StartCoroutine("ActionProfile_TurnRight", 90);
            //ActionProfile_RobotDrive(1, 0);
        if (Input.GetKeyDown("c"))
        {
            cam1.enabled = !cam1.enabled;
            cam2.enabled = !cam2.enabled;
            Debug.Log("c is pressed.");
        }*/
        
        //===========================================

        /*TODO: update robot according to specific motor actions vs. general action profile*/
        switch (currentControlIndex)
        {
            case ACTION_PROFILE_STOP:
                ActionProfile_Stop();
                movementSpeed = 0;
                break;
            case ACTION_PROFILE_FORWARD:
                ActionProfile_MoveForward();
                break;
            case ACTION_PROFILE_BACKWARD:
                ActionProfile_MoveBackward();
                break;
            case ACTION_PROFILE_LEFT:
				ActionProfile_Stop();
                StartCoroutine("ActionProfile_TurnLeft", 1);
                movementSpeed = 1;
                currentControlIndex = 0;
                break;
            case ACTION_PROFILE_RIGHT:
				ActionProfile_Stop();
                StartCoroutine("ActionProfile_TurnRight", 1);
                movementSpeed = 1;
                currentControlIndex = 0;
                break;
            case ACTION_PROFILE_TURN:
				ActionProfile_Stop();
                StartCoroutine("ActionProfile_TurnDegrees", Mathf.RoundToInt(currentTurnDegreeProfile));
                movementSpeed = 1;
                currentControlIndex = 0;
                break;
            case ACTION_PROFILE_TURNHEAD:
                StartCoroutine("ActionProfile_TurnHead", Mathf.RoundToInt(currentTurnDegreeProfile));
                movementSpeed = 1;
                currentControlIndex = 0;
                break;
            case ACTION_PROFILE_ROBOTDRIVE:
				ActionProfile_Stop();
                ActionProfile_RobotDrive(servoValues[0], servoValues[1]);
                movementSpeed = 1;
                break;
            default:
                currentControlIndex = 0;
                break;
        }
        if(scene.name == "Level01") anim.SetFloat("Speed", movementSpeed);

        //sendSensorUpdateProfile();
    }

    private class StateObjClass
    {
        // Used to hold parameters for calls to TimerTask.
        //public int SomeValue;
        public System.Threading.Timer TimerReference;
        public bool TimerCanceled;
    }

    private void sendSensorUpdateProfile(System.Object state)
    {
        StateObjClass stateObjClass = (StateObjClass)state;
        if (tcpContext != null)
        {

            // Update Robot Sensor Profile
            robotBean.sensorProfile = new System.Collections.Generic.List<SensorBean>();
            robotBean.actionProfile = incomingTcpMessage;

            float mDistance = distanceSensorRightController.distance;
            SensorBean distanceSensor = new SensorBean(1, "distance", "Laser");
            distanceSensor.readout.Add(new SensorReadoutBean("m", "distance", mDistance.ToString()));

            float mDistanceFront = distanceSensorFrontController.distance;
            SensorBean distanceSensorFront = new SensorBean(2, "distance", "Laser");
            distanceSensorFront.readout.Add(new SensorReadoutBean("m", "distance", mDistanceFront.ToString()));

			float mDistanceLeft = distanceSensorLeftController.distance;
			SensorBean distanceSensorLeft = new SensorBean(3, "distance", "Laser");
			distanceSensorLeft.readout.Add(new SensorReadoutBean("m", "distance", mDistanceLeft.ToString()));

			float mDistanceReverse = distanceSensorReverseController.distance;
			SensorBean distanceSensorReverse = new SensorBean(4, "distance", "Laser");
			distanceSensorReverse.readout.Add(new SensorReadoutBean("m", "distance", mDistanceReverse.ToString()));

            robotBean.sensorProfile.Add(distanceSensor);
            robotBean.sensorProfile.Add(distanceSensorFront);
			robotBean.sensorProfile.Add(distanceSensorLeft);
			robotBean.sensorProfile.Add(distanceSensorReverse);
            String strRobot = Regex.Replace(robotBean.toJSON().Trim(), @"\s+", "");
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(strRobot + "\n");

            output.Write(buffer, 0, buffer.Length);
        }
        else
        {
            stateObjClass.TimerReference.Dispose();
        }
    }

    /*
     *  SIMPLE DRIVING ROBOT ACTION PROFILE
     */

    public float angleRotationSpeed = 40.0f;
    public bool isRotating = false;
    public float rotatefactor = 5.5f;

    void ActionProfile_Stop()
    {
        transform.Translate(new Vector3(0, 0, 0));
    }

    void ActionProfile_MoveForward()
    {
        if (!isRotating)
        {
           // movementSpeed = 1;
            transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
        }
    }
    void ActionProfile_MoveBackward()
    {
        if (!isRotating)
        {
            //movementSpeed = -1;
            transform.Translate(-1 * Vector3.back * movementSpeed * Time.deltaTime);
        }
    }

    IEnumerator ActionProfile_TurnDegrees(int degreesToTurn)
    {
        if (degreesToTurn > 0)
        {
            StartCoroutine("ActionProfile_TurnRight", Math.Abs(degreesToTurn));
            //ActionProfile_TurnLeft(Math.Abs(degreesToTurn));
        }
        else if (degreesToTurn < 0)
        {
            StartCoroutine("ActionProfile_TurnLeft", Math.Abs(degreesToTurn));
            //ActionProfile_TurnRight(Math.Abs(degreesToTurn));
        }
        yield return null;
    }

    IEnumerator ActionProfile_TurnLeft(int degreesToTurn)
    {
        currentAbsoluteAngle = currentAbsoluteAngle - degreesToTurn;
        if (!isRotating)
        {
            isRotating = true;
            int newAngle = Mathf.RoundToInt(transform.eulerAngles.y - degreesToTurn) % 360;
            float deltaAngle = 0f;
            float currentTransform = transform.eulerAngles.y;

            while (deltaAngle < Math.Abs(Math.Abs(degreesToTurn) - 0.5f))
            {
                float updatedEulAngley = Mathf.MoveTowards(transform.eulerAngles.y, newAngle, angleRotationSpeed * Time.deltaTime);
                deltaAngle += Math.Abs(updatedEulAngley - transform.eulerAngles.y) % 360;
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, updatedEulAngley);
                yield return null;
            }
            //transform.eulerAngles = new Vector3(transform.eulerAngles.x, degree);
            isRotating = false;
        }
    }

    IEnumerator ActionProfile_TurnRight(int degreesToTurn)
    {
        currentAbsoluteAngle = currentAbsoluteAngle + degreesToTurn;
        if (!isRotating)
        {
            isRotating = true;
            int newAngle = Mathf.RoundToInt(transform.eulerAngles.y + degreesToTurn);
            float deltaAngle = 0f;
            float currentTransform = transform.eulerAngles.y;

            while (deltaAngle < Math.Abs(Math.Abs(degreesToTurn) - 0.5f))
            {
                float updatedEulAngley = Mathf.MoveTowards(transform.eulerAngles.y, newAngle, angleRotationSpeed * Time.deltaTime);
                deltaAngle += Math.Abs(updatedEulAngley - transform.eulerAngles.y) % 360;
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, updatedEulAngley);
                yield return null;
            }
            isRotating = false;
        }
    }

    void ActionProfile_RobotDrive(float left, float right)
    {
        float deltaAngle = 0f;
        float differential = (left - right) / 2;
        float updatedEulAngley = Mathf.MoveTowards(transform.eulerAngles.y, transform.eulerAngles.y + differential * 1000, rotatefactor * angleRotationSpeed * Time.deltaTime * Math.Abs(differential));
        //deltaAngle += Math.Abs(updatedEulAngley - transform.eulerAngles.y) % 360;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, updatedEulAngley);

        movementSpeed = (left + right) / 2;
        transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);

    }

    public void resetRobot()
    {
        currentAbsoluteAngle = 0;
        // stop the robot
        transform.Translate(new Vector3(0f, 0f, 0f));
        // reset position of robot
        transform.position = new Vector3(2.49871f, 0f, 3.07641f);
        // reset angle of robot
        transform.eulerAngles = new Vector3(-0.044f, -4.52f, -0.396f);
		setControlFlag(false);
		StopAllCoroutines();
		movementSpeed = 0;
        // reset robot variables
        currentControlIndex = 0;
        currentTurnDegreeProfile = 0f;

        IsManual = false;

        this.StopCoroutine("ActionProfile_TurnLeft");
        this.StopCoroutine("ActionProfile_TurnRight");
        this.StopCoroutine("ActionProfile_TurnDegrees");
    }

    public void readjustRobot()
    {
        if (!isRotating)
        {
            // stop the robot
            //transform.Translate(new Vector3(0f, 0f, 0f));
            // reset angle of robot
            transform.eulerAngles = new Vector3(0f, (float)currentAbsoluteAngle, 0f);
        }


        //stop turning routines
        /*this.StopCoroutine("ActionProfile_TurnLeft");
        this.StopCoroutine("ActionProfile_TurnRight");
        this.StopCoroutine("ActionProfile_TurnDegrees");*/
    }

    public void switchRobot()
    {
        if (scene.name == "Level01")
        {
            tcpListener.Stop();
            //Application.LoadLevel("Level02");
            //sceneFlag = false;
        }
        else
        {
            tcpListener.Stop();
            //Application.LoadLevel("Level01");
            //sceneFlag = true;
        }
    }

    public void switchCamera()
    {
        if(cam1.enabled == true)
        {
            cam1.enabled = false;
            cam2.enabled = true;
        }
        else if(cam2.enabled == true)
        {
            cam2.enabled = false;
            cam3.enabled = true;
        }
        else if(cam3.enabled == true)
        {
            cam3.enabled = false;
            cam1.enabled = true;
        }
    }


    private bool toggleTxt = false;

    private void OnGUI()
    {
        if(toggleTxt = GUI.Toggle(new Rect(10, 140, 100, 30), toggleTxt, "Manual Control"))
        {
            if (GUI.Button(new Rect(70, 200, 50, 30), "Stop"))
            {
                IsManual = true;
                manProfile = 0;
            }
            if (GUI.Button(new Rect(70, 160, 70, 30), "Forward"))
            {
                IsManual = true;
                manProfile = 1;
            }
            if (GUI.Button(new Rect(70, 240, 50, 30), "Back"))
            {
                IsManual = true;
                manProfile = 2;
            }
            if (GUI.Button(new Rect(10, 200, 50, 30), "Left"))
            {
                IsManual = true;
                manProfile = 3;
            }
            if (GUI.Button(new Rect(130, 200, 50, 30), "Right"))
            {
                IsManual = true;
                manProfile = 4;
            }

            if (GUI.Button(new Rect(10, 300, 100, 30), "No Manual"))
            {
                IsManual = false;
            }
        }

        if (sceneFlag)
        {
            if (GUI.Button(new Rect(10, 20, 100, 30), "Switch Robot"))
            {
                if (scene.name == "Level01")
                {
                    tcpListener.Stop();
                    //Application.LoadLevel("Level02");
                }
                else
                {
                    tcpListener.Stop();
                    //Application.LoadLevel("Level01");
                }
            }
        }
        
        if (GUI.Button(new Rect(10, 60, 100, 30), "Reset"))
        {
            resetRobot();
            sceneFlag = true;
        }

        if (GUI.Button(new Rect(10, 100, 100, 30), "Switch Camera"))
        {
            switchCamera();
        }		
    }
    public void setManProfile(int v)
    {
        manProfile = v;
        return;
    }
    public void setManual(Boolean f)
    {
        IsManual = f;
        return;
    }


    //================================================================
    //Another way to control the robot
    //================================================================


    



    public void optionCodeControl()
    {
        int if1, func1,hideCnt,if2,if3,if4,func2,func3,func4;
        float value1,value2,value3,value4;
        float mDistanceFront = distanceSensorFrontController.distance;
        float mDistanceReverse = distanceSensorReverseController.distance;
        float mDistanceLeft = distanceSensorLeftController.distance;
        float mDistanceRight = distanceSensorRightController.distance;
        if1 = optionClass.getIf1();
        func1 = optionClass.getFunc1();
        value1 = optionClass.getValue1();

        if2 = optionClass.if2;
        func2 = optionClass.func2;
        value2 = optionClass.value2;

        if3 = optionClass.if3;
        func3 = optionClass.func3;
        value3 = optionClass.value3;

        if4 = optionClass.if4;
        func4 = optionClass.func4;
        value4 = optionClass.value4;
        hideCnt = optionClass.getHideCnt();

        if (hideCnt == 0)
        {
            defaultFunc();
        }
        if (hideCnt >=1)
        {
            if (mySwitch(if1,value1,func1)==true)
            {
                if (hideCnt >= 2)
                {
                    if (mySwitch(if2, value2, func2) == true)
                    {
                        if(hideCnt >= 3)
                        {
                            if(mySwitch(if3, value3, func3) == true)
                            {
                                if(hideCnt >= 4)
                                {
                                    if(mySwitch(if4, value4, func4) == true)
                                    {
                                        defaultFunc();
                                    }
                                }
                                else
                                {
                                    defaultFunc();
                                }
                            }
                        }
                        else
                        {
                            defaultFunc();
                        }
                    }
                }
                else
                {
                    defaultFunc();
                }
            }
        }

        /*                if (hideCnt >= 2 && mySwitch(if2, value2, func2) == true)
                {
                    if (hideCnt >= 3 && mySwitch(if3, value3, func3) == true)
                    {
                        if (hideCnt >= 4 && mySwitch(if4, value4, func4))
                        {
                            defaultFunc();
                        }
                    }
                }*/

        /*
        if (hideCnt >= 1)
        {

            switch(if1)
            {
                case 0://if sensor.forward >
                    if (mDistanceFront > value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2,value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
                case 1://if sensor.forward <
                    if (mDistanceFront < value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2, value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
                case 2://if sensor.reverse >
                    if (mDistanceReverse > value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2, value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
                case 3://if sensor.reverse <
                    if (mDistanceReverse < value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2, value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
                case 4://if sensor.left >
                    if (mDistanceLeft > value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2, value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
                case 5://if sensor.left <
                    if (mDistanceLeft < value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2, value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
                case 6://if sensor.right >
                    if (mDistanceRight > value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2, value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
                case 7://if sensor.right <
                    if (mDistanceRight < value1)
                    {
                        changeFunc(func1);
                    }
                    else if (hideCnt == 2)
                    {
                        mySwitch(if2, value2, func2);
                    }
                    else if (hideCnt == 3)
                    {
                        mySwitch(if3, value3, func3);
                    }
                    else if (hideCnt >= 4)
                    {
                        mySwitch(if4, value4, func4);
                    }
                    else
                    {
                        defaultFunc();
                    }
                    break;
            }
        }
        else
        {
            defaultFunc();
        }
        */

    }

    public bool mySwitch(int myIf,float myValue, int myFunc)
    {
        bool nextIf = false;
        float mDistanceFront = distanceSensorFrontController.distance;
        float mDistanceReverse = distanceSensorReverseController.distance;
        float mDistanceLeft = distanceSensorLeftController.distance;
        float mDistanceRight = distanceSensorRightController.distance;

        switch(myIf)
        {
            case 0://if sensor.forward >
                if (mDistanceFront > myValue)
                {
                    changeFunc(myFunc);
                }
                else
                {
                    nextIf = true;
                }
                break;
            case 1://if sensor.forward <
                if (mDistanceFront < myValue)
                {
                    changeFunc(myFunc);
                }
                else
                {
                    nextIf = true;
                }
                break;
            case 2://if sensor.reverse >
                if (mDistanceReverse > myValue)
                {
                    changeFunc(myFunc);
                }
                else
                {
                    nextIf = true;
                }
                break;
            case 3://if sensor.reverse <
                if (mDistanceReverse < myValue)
                {
                    changeFunc(myFunc);
                }
                else
                {
                    nextIf = true;
                }
                break;
            case 4://if sensor.left >
                if (mDistanceLeft > myValue)
                {
                    changeFunc(myFunc);
                }
                else
                {
                    nextIf = true;
                }
                break;
            case 5://if sensor.left <
                if (mDistanceLeft < myValue)
                {
                    changeFunc(myFunc);
                }
                else
                {
                    nextIf = true;
                }
                break;
            case 6://if sensor.right >
                if (mDistanceRight > myValue)
                {
                    if (delayFlag == false)
                    {
                        changeFunc(myFunc);
                    }
                    else
                    {
                        defaultFunc();
                    }
                }
                else
                {
                    nextIf = true;
                }
                break;
            case 7://if sensor.right <
                if (mDistanceRight < myValue)
                {
                    changeFunc(myFunc);
                }
                else
                {
                    nextIf = true;
                }
                break;
        }
        return nextIf;
    }

    public void ActionProfile_TurnLeftRaw()
    {
        StartCoroutine("ActionProfile_TurnLeft", 90);
    }

    public void ActionProfile_TurnRightRaw()
    {
        StartCoroutine("ActionProfile_TurnRight", 90);
    }

    public void ActionProfile_MoveForwardRaw()
    {
        StartCoroutine("ActionProfile_MoveForward");
    }

    public void changeDelayFlag()
    {
        delayFlag = false;
    }

    public void changeFunc(int i)
    {
        switch(i){
            case 0:
                movementSpeed = 1;
                ActionProfile_MoveForward();
                if (scene.name == "Level01") anim.SetFloat("Speed", 1);
                break;
            case 1:
                if (scene.name == "Level01") anim.SetFloat("Speed", 1);
                movementSpeed = -1;
                ActionProfile_MoveBackward();
                break;
            case 2:  //Delay turn case

                if (scene.name == "Level01") anim.SetFloat("Speed", 1);
                //ActionProfile_MoveForward();
                //Invoke("ActionProfile_TurnLeftRaw", 1);
                if (delayFlag == true)
                {
                    break;
                }
                delayFlag = true;
                StartCoroutine("ActionProfile_TurnLeft", 90);
                Invoke("changeDelayFlag", 10);
                ActionProfile_MoveForward();
                break;
            case 3:
                if (delayFlag == true)
                {
                    break;
                }
                if (scene.name == "Level01") anim.SetFloat("Speed", 1);
                movementSpeed = 0;
                StartCoroutine("ActionProfile_TurnLeft", 90);
                break;
            case 4:
                //Delay turn right 90
                //if (scene.name == "Level01") anim.SetFloat("Speed", 1);
                //ActionProfile_MoveForward();
                //Invoke("ActionProfile_TurnRightRaw", 1);
                if (delayFlag == true)
                {
                    ActionProfile_MoveForward();
                    break;
                }
                delayFlag = true;
                StartCoroutine("ActionProfile_TurnRight", 90);
                Invoke("changeDelayFlag",6);
                break;
            case 5:
                if (delayFlag == true)
                {
                    break;
                }
                if (scene.name == "Level01") anim.SetFloat("Speed", 1);
                movementSpeed = 0;
                StartCoroutine("ActionProfile_TurnRight", 90);
                break;
            case 6:
                if (delayFlag == true)
                {
                    break;
                }
                if (scene.name == "Level01") anim.SetFloat("Speed", 1);
                movementSpeed = 0;
                StartCoroutine("ActionProfile_TurnRight", 90);
                break;
        }
    }

    public void defaultFunc()
    {
        if(optionClass.default1 == 0)
        {
            ActionProfile_Stop();
            if (scene.name == "Level01") anim.SetFloat("Speed", 0);
            movementSpeed = 0;
        }
        else if(optionClass.default1 == 1)
        {
            movementSpeed = 1;
            ActionProfile_MoveForward();
            if (scene.name == "Level01") anim.SetFloat("Speed", 1);
        }
        else if(optionClass.default1 == 2)
        {
            if (scene.name == "Level01") anim.SetFloat("Speed", 1);
            movementSpeed = -1;
            ActionProfile_MoveBackward();
        }
        else if(optionClass.default1 == 3)
        {
            if (scene.name == "Level01") anim.SetFloat("Speed", 1);
            ActionProfile_Stop();
            StartCoroutine("ActionProfile_TurnLeft", 1);
        }
        else if(optionClass.default1 == 4)
        {
            if (scene.name == "Level01") anim.SetFloat("Speed", 1);
            ActionProfile_Stop();
            StartCoroutine("ActionProfile_TurnRight", 90);
        }
    }

}
