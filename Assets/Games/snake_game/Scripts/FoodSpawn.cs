﻿using UnityEngine;
using System.Collections;

public class FoodSpawn : MonoBehaviour
{
    public GameObject foodPrefab;
    
    public int gridRadius;

    public int maxFood;

    private int foodCount;
    
    void Start()
    {
        foodCount = 0;
        InvokeRepeating("Spawn", 1, 3);
    }

    public void SnakeAteFood()
    {
        if (foodCount > 0)
        {
            foodCount = foodCount - 1;
        }
    }
    
    void Spawn()
    {
        int x = (int)Random.Range(-gridRadius, gridRadius);
        
        int z = (int)Random.Range(-gridRadius, gridRadius);
        
        if (foodCount <= maxFood)
        {
            Instantiate(foodPrefab,
                        new Vector3(x, .5f, z),
                        Quaternion.identity);
            foodCount++;
        }
    }
}