﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorScript : MonoBehaviour {

    RaycastHit hit;

    Vector3 directionVector;

    public void Start()
    {
        // initial cube begins facing right
        directionVector = transform.TransformDirection(Vector3.right);
    }

    public void updateRotation()
    {
        directionVector = transform.TransformDirection(Vector3.right);
    }

    public void checkDistance()
    {
        if (Physics.Raycast(transform.position + directionVector, directionVector, out hit, 20.0f))
            Debug.Log(tag + " distance: " + hit.distance);
    }
}
