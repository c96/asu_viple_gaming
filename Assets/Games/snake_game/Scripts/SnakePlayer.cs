﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakePlayer : MonoBehaviour {

    public float speed = 1.5f;

    public Vector3 dir = Vector2.right;
    public Vector3 nextDir = Vector2.right;

    public GameObject headPrefab;
    public GameObject tailPrefab;

    public bool alive = true;

    // Use this for initialization
    void Start () {
        //start live state manually later
        alive = false;
	}
	
	// Update is called once per frame
	void Update () {

        ReadInputAndRotate();
        Move();
    }

    void ReadInputAndRotate()
    {
        /*
        if (Input.GetAxis("Horizontal") < 0)
        {
            nextDir = Vector3.left;
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            nextDir = Vector3.right;
        }
        if (Input.GetAxis("Vertical") > 0)
        {
            nextDir = Vector3.forward;
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            nextDir = Vector3.back;
        }
        */

        if (Input.GetKey(KeyCode.G))
        {
            alive = true;
        }
    }

    void Move()
    {
        if (alive)
        {
            transform.position += nextDir * speed * Time.deltaTime;
        }
    }
}
