﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleSensorReadout : MonoBehaviour {
	public Text forward,back,left,right,servo0,servo1;
	public RobotController robo;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		forward.text = "Front Sensor: "+ robo.distanceSensorFrontController.distance;
		back.text = "Back Sensor: "+ robo.distanceSensorReverseController.distance;
		left.text = "Left Sensor: "+ robo.distanceSensorLeftController.distance;
		right.text = "Right Sensor: "+ robo.distanceSensorRightController.distance;
		servo0.text = "Servo 0: " + robo.servoValues [0];
		servo1.text = "Servo 1: " + robo.servoValues [1];

	}
}
