﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawn : MonoBehaviour {

    public GameObject obstaclePrefab;

    public float baseSpawnDistanceRight = 10.0f;
    public float baseSpawnHeight = .76f;

    void Start()
    {
        InvokeRepeating("Spawn", 1, 3);
    }

    void Spawn()
    {
        Instantiate(obstaclePrefab,
                    new Vector2(baseSpawnDistanceRight, baseSpawnHeight),
                    Quaternion.identity);
    }
}
