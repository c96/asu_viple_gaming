﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour {

    void Start()
    {
        InvokeRepeating("Move", 1, 0.05f);
    }

    void Move()
    {
        transform.position += Vector3.left*0.12f;
    }
}
